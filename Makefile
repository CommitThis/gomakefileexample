.PHONY: all
G_OBJ_DIR = golang/src
G_SRC_DIR = golang/src

G_SRCS := $(shell find $(G_SRC_DIR) -name '*.go')
G_OBJS = $(patsubst $(G_SRC_DIR)%,$(G_OBJ_DIR)%, $(G_SRCS:.go=.o))

default: all

all: testgo

objs: $(G_OBJS)

$(G_SRC_DIR)/committhis/%.o: $(G_SRC_DIR)/committhis/%.go
	gccgo -c $^ -o $@

testgo: objs
	gccgo -c -Igolang/src testgohello.go
	gccgo -o testgohello testgohello.o $(G_OBJS)


clean:
	-rm -rf $(G_OBJS)
	-rm -rf testgohello.o
	-rm -rf testgohello
