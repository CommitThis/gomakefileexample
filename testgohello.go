package main

import "fmt"
import "committhis/foo"
import "committhis/bar"


func main() {
    fmt.Println("Hello")
    foo.Foo()
    bar.Bar()
}
